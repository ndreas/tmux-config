#!/usr/bin/env bash

if (($(tmux list-panes | wc -l) > 1)); then
    tmux display-message 'Replace is only available for sessions with a single pane'
fi

session_id=$(tmux display-message -p '#{session_id}')

tmux choose-tree -Zs "switch-client -t '%%'; if-shell -F '#{s/\\${session_id}//:session_id}' 'kill-session -t ${session_id}'"
