#!/bin/sh

pane_path=$1
shift

if ! command -v git >/dev/null 2>&1; then
    tmux $* -c "${pane_path}"
fi

git_root=$(git -C "${pane_path}" rev-parse --show-toplevel)
res="$?"
if [ "$res" -eq 0 ]; then
    tmux $* -c "${git_root}"
else
    tmux $* -c "${pane_path}"
fi
