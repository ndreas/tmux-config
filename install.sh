#!/bin/sh
tmux_dir=$(CDPATH='' cd -- "$(dirname "$0")" && pwd)
tmux_conf="${HOME}/.tmux.conf"

if [ -L "$tmux_conf" ]; then
    rm "$tmux_conf"
fi

echo "source-file ${tmux_dir}/tmux.conf" >"$tmux_conf"
